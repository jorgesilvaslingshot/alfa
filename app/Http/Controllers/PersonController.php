<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\Repository;
use App\Person;
use App\Contacts;
use App\Http\Requests\StorePersonn;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Request;

class PersonController extends Controller
{
    public function __construct(Person $model)
    {
        $this->model = new Repository($model);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $person = $this->model->all(null,9);

        return view('person.index')->with('person', $person);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $person = new Person();

        return view('person.edit', compact('person'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id=null)
    {
        $validatedData = $request->validate([
            //'email' => 'required|email',
            'name' => 'required',
            'email' =>  [
                'email',
                'required', 
            ]
        ]);

        $request = $request->except('_token');

        if($id){
            $saved = $this->model->update($request, $id);
            $msg = "Contact updated";
        }else{
            $saved  = $this->model->create($request);
            $msg = "Contact created";
        }

        return redirect(route('person'))->with('success', $msg);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\equipas  $equipas
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $person = $this->model->find($id);

        $contactos = Contacts::where('person_id', $id)->get();

        return view('person.edit', compact(['person', 'contactos']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\equipas  $equipas
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Equipas  $Equipas
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $model = $this->model->delete($id);

        return redirect()->back();
    }

}
