<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreContactos;
use App\Contacts;
use App\Repositories\Repository;
use Illuminate\Http\Request;
use App\Person;

class ContactosController extends Controller
{
    public function __construct(Contacts $model)
    {
        $this->model = new Repository($model);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactos = $this->model->all(null,9);

        return view('contactos.index')->with('contactos', $contactos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $contacto = new Contacts();

        $person = Person::all();

        return view('contactos.edit', compact('contacto', 'person'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id=null)
    {
        $request = $request->except('_token');

        if($id){
            $saved = $this->model->update($request, $id);
            $msg = "Contact updated";
        }else{
            $saved  = $this->model->create($request);
            $msg = "Contact created";
        }

        return redirect(route('contactos'))->with('success', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\equipas  $equipas
     * @return \Illuminate\Http\Response
     */
    public function show(Equipas $equipas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\equipas  $equipas
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contacto = $this->model->find($id);

        $person = Person::all();

        return view('contactos.edit', compact('contacto', 'person'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\equipas  $equipas
     * @return \Illuminate\Http\Response
     */
    public function update(StoreEquipas $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Equipas  $Equipas
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $model = $this->model->delete($id);

        return redirect(route('contactos'))->with('success', 'Contact deleted');
    }

}
