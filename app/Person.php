<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Person extends Model
{
    use SoftDeletes;

    public $table = "person";


    protected $fillable = ['id', 'name', 'email'];

    protected $casts = [
        'created_at' => 'datetime:d/m/Y',
        'updated_at' => 'datetime:d/m/Y',
    ];

    public function contactos() : HasMany
    {
        return $this->hasMany(Contacts::class, 'person_id');
    }

    public function getContactos()
    {
        return $this->equipas;
    }
}
