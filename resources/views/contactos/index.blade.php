@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            @endif

            <a href="{{route('contactos.create')}}" class="btn btn-primary">Create Contact</a>
            <br>
            <table class="table table-striped mt-4">
                <tr>
                    <th>ID</th>
                    <th>Country code</th>
                    <th>Number</th>
                    <th>Person</th>
                    <th></th>
                    <th></th>
                </tr>

                @foreach($contactos as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->countrycode}}</td>
                    <td>{{$item->number}}</td>
                    <td>{{$item->person->name}}</td>
                    <td><a href="{{route('contactos.edit', $item->id)}}">edit</a></td>
                    <td><a href="{{route('contactos.delete', $item->id)}}">delete</a></td>
                </tr>
                @endforeach


            </table>
        </div>
    </div>
</div>
@endsection