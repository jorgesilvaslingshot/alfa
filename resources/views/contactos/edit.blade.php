@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif

            <form action="{{route('contactos.store', $contacto->id)}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="countrycode">Country Code</label>
                    <!-- <select class="countrycode form-control" name="countrycode" id="#countrycode" value="{{$contacto->countrycode}}">
                    </select> -->
                    <!-- <input type="number" class="form-control" name="countrycode" id="countrycode" value="{{$contacto->countrycode}}"> -->

                    <div class="select-vue">
                        <select-component input-name="countrycode" input-value="{{$contacto->countrycode ? $contacto->countrycode : 0}}"></select-component>
                    </div>

                </div>
                <div class="form-group">
                    <label for="fnumber">Number</label>
                    <input type="number" class="form-control" required name="number" id="fnumber" value="{{$contacto->number}}">

                </div>

                <div class="form-group">
                    <label for="person_id">Person</label>
                    <select class="form-control" name="person_id" id="person_id" value="{{$contacto->person_id}}" required>
                        <option value="" {{$contacto->person_id ? '' : 'selected'}}>Select a person</option>
                    @foreach($person as $item) 
                    <option value="{{$item->id}}" {{$item->id == $contacto->person_id ? 'selected': ''}}>
                        {{$item->name}}
                        </option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

        </div>
    </div>
</div>

@endsection