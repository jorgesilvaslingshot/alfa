@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif

            <div class="row">

                <form action="{{route('person.store', $person->id)}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name</label>
                        <!-- <select class="countrycode form-control" name="countrycode" id="#countrycode">
                    </select> -->
                        <input type="text" class="form-control" name="name" id="name" value="{{$person->name}}">


                    </div>
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" class="form-control" name="email" id="email" value="{{$person->email}}">

                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>

            @if(isset($contactos))
            <div class="row mt-4">
                <table class="table table-striped mt-4">
                    <tr>
                        <th>ID</th>
                        <th>Country code</th>
                        <th>Number</th>
                        <th></th>
                        <th></th>
                    </tr>

                    @foreach($contactos as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->countrycode}}</td>
                        <td>{{$item->number}}</td>
                        <td><a href="{{route('contactos.edit', $item->id)}}">edit</a></td>
                        <td><a href="{{route('contactos.delete', $item->id)}}">delete</a></td>
                    </tr>
                    @endforeach


                </table>
            </div>
            @endif

        </div>
    </div>
</div>

@endsection