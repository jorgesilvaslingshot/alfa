@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

        @if(session()->get('success'))
            <div class="alert alert-success">
            {{ session()->get('success') }}  
            </div>
        @endif

        <a href="{{route('person.create')}}" class="btn btn-primary">Create Person</a>
            
        <table class="table table-striped mt-4">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>E-mail</th>
                <th></th>
                <th></th>
            </tr>

            @foreach($person as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->email}}</td>
                <td><a href="{{route('person.edit', $item->id)}}">edit</a></td>
                <td><a href="{{route('person.destroy', $item->id)}}">delete</a></td>
            </tr>
            @endforeach

            
        </table>
        </div>
    </div>
</div>
@endsection
