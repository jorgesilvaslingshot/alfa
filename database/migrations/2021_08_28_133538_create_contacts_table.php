<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('countrycode');
            $table->integer('number');
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
            //$table->bigInteger('person_id')->unsigned();
            $table->integer('person_id')->unsigned();

        });

        // Schema::table('contacts', function($table) {
        //     $table->foreign('person_id')->references('id')->on('person');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
