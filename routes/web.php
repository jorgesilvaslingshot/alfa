<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ContactosController@index')->name('contactos');

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::group(['prefix' => 'contactos'], function () {

        Route::get('create', 'ContactosController@create')->name('contactos.create');

        Route::get('edit/{id?}', 'ContactosController@edit')->name('contactos.edit');

        Route::post('edit/{id?}', 'ContactosController@store')->name('contactos.store');

        Route::get('delete/{id}', 'ContactosController@delete')->name('contactos.delete');

        //person
    });

    Route::resource('person', PersonController::class);

    
});
